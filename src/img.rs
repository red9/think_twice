// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use kd_tree;
use typenum;

// a CGA palette
pub const CGA: [[f64; 3]; 16] = [
    [0.0, 0.0, 0.0],
    [0.0, 0.0, 0.666],
    [0.0, 0.666, 0.0],
    [0.0, 0.666, 0.666],
    [0.666, 0.0, 0.0],
    [0.666, 0.0, 0.666],
    [0.666, 0.333, 0.0],
    [0.666, 0.666, 0.666],
    [0.333, 0.333, 0.333],
    [0.333, 0.333, 1.0],
    [0.333, 1.0, 0.333],
    [0.333, 1.0, 1.0],
    [1.0, 0.333, 0.333],
    [1.0, 0.333, 1.0],
    [1.0, 1.0, 0.333],
    [1.0, 1.0, 1.0],
];

pub const LUM_RED_FACTOR: f64 = 0.2126;
pub const LUM_GREEN_FACTOR: f64 = 0.7152;
pub const LUM_BLUE_FACTOR: f64 = 0.0722;

#[derive(Clone, Copy)]
pub struct Colour {
    pub colour: [f64; 3],
    pub colour_gamma: [f64; 3],
    pub luminance: f64,
}

impl Colour {
    // transforms a slice of length 3 into a Colour
    pub fn new(colour: &[f64]) -> Colour {
        let colour = [colour[0], colour[1], colour[2]];
        let colour_gamma = linearise3(colour);
        let luminance = luminance(colour_gamma);
        Colour{colour, colour_gamma, luminance}
    }

    pub fn from_gamma(colour: &[f64]) -> Colour {
        let colour_gamma = [colour[0], colour[1], colour[2]];
        let colour = delinearise3(colour_gamma);
        let luminance = luminance(colour_gamma);
        Colour{colour, colour_gamma, luminance}
    }

    pub fn dist_sq(&self, other: &Colour) -> f64 {
        let d_r = self.colour_gamma[0] - other.colour_gamma[0];
        let d_b = self.colour_gamma[1] - other.colour_gamma[1];
        let d_g = self.colour_gamma[2] - other.colour_gamma[2];
        let d_lum = self.luminance - other.luminance;
        d_r*d_r + d_g*d_g + d_b*d_b + d_lum*d_lum
    }

    pub fn closest<'a>(&self, palette: &'a [Colour]) -> &'a Colour {
        let mut least_error: f64 = (1 << 16) as f64;
        let mut least_error_idx: usize = 0;
        for i in 0..palette.len() {
            let pal_colour = &palette[i];
            let error = self.dist_sq(pal_colour);
            if error < least_error {
                least_error = error;
                least_error_idx = i;
            }
        }
        &palette[least_error_idx]
    }

    pub fn eq(&self, other: &Colour) -> bool {
        self.dist_sq(other) < 10.0_f64.powi(-10)
    }
}

impl kd_tree::KdPoint for Colour {
    type Scalar = f64;
    type Dim = typenum::U4;
    fn at(&self, k: usize) -> f64 {
        if k < 3 {
            self.colour_gamma[k]
        } else {
            self.luminance
        }
    }
}

pub struct Palette {
    pub kdtree: kd_tree::KdTree<Colour>,
}

impl Palette {
    pub fn new(colours: Vec<Colour>) -> Palette {
        Palette{kdtree: kd_tree::KdTree::build_by_ordered_float(colours)}
    }

    pub fn nearest(&self, colour: &Colour) -> &Colour {
        let nearest = self.kdtree.nearest(colour).unwrap().item;
        return nearest
    }

    pub fn len(&self) -> usize {
        self.kdtree.len()
    }
}

fn colour_pt(colour: &Colour) -> [f64; 4] {
    [colour.colour_gamma[0], colour.colour_gamma[1], colour.colour_gamma[2], colour.luminance]
}
pub fn linearise3(colour: [f64; 3]) -> [f64; 3] {
    let mut l = [0.0, 0.0, 0.0];
    for n in 0..3 {
        l[n] = linearise(colour[n])
    }
    l
}
pub fn linearise(colour: f64) -> f64 {
    if colour <= 0.04045 {
        colour / 12.92
    } else {
        ((colour + 0.055)/1.055).powf(2.4)
    }
}
pub fn delinearise3(colour: [f64; 3]) -> [f64; 3] {
    let mut nl = [0.0, 0.0, 0.0];
    for n in 0..3 {
        nl[n] = delinearise(colour[n])
    }
    nl
}
pub fn delinearise(colour: f64) -> f64 {
    if colour <= 0.0031318 {
        12.92*colour
    } else {
        1.055*colour.powf(1./2.4) - 0.055
    }
}
pub fn luminance(colour: [f64; 3]) -> f64 {
    colour[0]*LUM_RED_FACTOR + colour[1]*LUM_GREEN_FACTOR
        + colour[2]*LUM_BLUE_FACTOR
}
