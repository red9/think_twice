// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::img::*;

// 8x8 bayer matrix
const BAYER_8: [f64; 64] = [
    0.0000, 0.7500, 0.1875, 0.9375, 0.0469, 0.7969, 0.2344, 0.9844,
    0.5000, 0.2500, 0.6875, 0.4375, 0.5469, 0.2969, 0.7344, 0.4844,
    0.1250, 0.8750, 0.0625, 0.8125, 0.1719, 0.9219, 0.1094, 0.8594,
    0.6250, 0.3750, 0.5625, 0.3125, 0.6719, 0.4219, 0.6094, 0.3594,
    0.0312, 0.7812, 0.2188, 0.9688, 0.0156, 0.7656, 0.2031, 0.9531,
    0.5312, 0.2812, 0.7188, 0.4688, 0.5156, 0.2656, 0.7031, 0.4531,
    0.1562, 0.9062, 0.0938, 0.8438, 0.1406, 0.8906, 0.0781, 0.8281,
    0.6562, 0.4062, 0.5938, 0.3438, 0.6406, 0.3906, 0.5781, 0.3281,
];

pub fn dither<'a, 'b>(img: &'b Vec<f64>, width: usize, palette: &'a Palette, dithering_factor: Option<f64>) -> Vec<&'a Colour> {
    let height = img.len()/(3*width);
    let mut colour_map = Vec::with_capacity(img.len()/3);
    let mut colour_dist_sum = 0.0;
    let k = match dithering_factor {
        Some(k) => k,
        None => {
            for n in 0..palette.len() {
                let c = palette.kdtree[n];
                let dist = palette.kdtree.nearests(&c, 2)[1].squared_distance.sqrt();
                colour_dist_sum += dist;
            }
            colour_dist_sum * 0.75 / palette.len() as f64
        }
    };
    println!("k: {}", k);
    for row in 0..height {
        for col in 0..width {
            let idx = row*width*3 + col*3;
            let mut colour = img[idx..idx+3].to_vec();
            for n in 0..3 { colour[n] += (BAYER_8[(8*row + col) % 64] - 0.5) * k }
            let new_colour = palette.nearest(&Colour::new(&*colour));
            colour_map.push(new_colour)
        }
    }
    colour_map
}