use std::cmp::Ordering;
use std::ops::Range;
use linfa::DatasetBase;
use ndarray::{Array, Array2, ArrayView};
use linfa::traits::Fit;
use linfa_clustering::{KMeans, KMeansParams, KMeansInit};
use crate::img::*;
use rand;
use rand::Rng;

pub fn random_palette(img: &Vec<f64>, length: usize) -> Palette {
    let img_size = img.len() / 3;
    let chunk_size = img_size / length;
    let mut palette: Vec<Colour> = Vec::with_capacity(length);
    let mut rng = rand::thread_rng();
    // we split the image into strips and randomly pick a colour from each
    for n in 0..length {
        let idx = (n * chunk_size + rng.gen_range(0..chunk_size)) * 3;
        let colour = Colour::new(&img[idx..idx+3]);
        palette.push(colour);
    }
    Palette::new(palette)
}

pub fn cga(_img: &Vec<f64>, _length: usize) -> Palette {
    let vec_colour = CGA.iter().map(|x| Colour::new(x)).collect();
    Palette::new(vec_colour)
}

pub fn median_cut(img: &Vec<f64>, length: usize) -> Palette {
    let mut image: Vec<&[f64]> = Vec::with_capacity(img.len()/3);
    for n in 0..img.len()/3 {
        let idx = n*3;
        let colour = &img[idx..idx+3];
        image.push(colour.clone())
    }
    let mut buckets = vec!(image.as_mut_slice());
    for _n in 1..length {
        buckets.sort_by(|a, b| a.len().cmp(&b.len()));
        let largest = buckets.pop().unwrap();
        if largest.len() == 1 { break }
        let mut mins = [largest[0][0], largest[1][0], largest[2][0]];
        let mut maxs = mins;
        for i in 1..largest.len(){
            for j in 0..3 {
                if mins[j] > largest[i][j] {mins[j] = largest[i][j]}
                if maxs[j] < largest[i][j] {maxs[j] = largest[i][j]}
            }
        }
        let mut max_range = 0.;
        let mut max_range_axis = 0;
        for i in 0..3 {
            let range = maxs[i] - mins[i];
            if max_range < range {max_range = range; max_range_axis = i}
        }
        largest.sort_by(
            |a, b| a[max_range_axis].partial_cmp(&b[max_range_axis]).unwrap_or(Ordering::Equal)
        );
        let (a, b) = largest.split_at_mut(largest.len()/2);
        buckets.push(a);
        buckets.push(b);
    }
    let mut colours : Vec<Colour> = Vec::with_capacity(length);
    for n in 0..buckets.len() {
        let mut sum = [0., 0., 0.];
        let bucket = &buckets[n];
        for i in 0..bucket.len() {
            for j in 0..3 {sum[j] += bucket[i][j]}
        }
        for j in 0..3 {sum[j] /= bucket.len() as f64}
        colours.push(Colour::new(&sum))
    }
    Palette::new(colours)
}

struct CSlice<'a> {
    pub colours: &'a mut [Colour],
    pub ranges: [Range<f64>; 3]
}
impl CSlice<'_> {
    fn from_colour_slice(colours: &mut [Colour]) -> CSlice {
        let c = colours[0];
        let mut ranges =
            [c.colour[0]..c.colour[0], c.colour[1]..c.colour[1], c.colour[2]..c.colour[2]];
        let mut cslice = CSlice {
            colours, ranges
        };
        cslice.recalculate_ranges();
        cslice
    }

    fn recalculate_ranges(&mut self) {
        let c = self.colours[0];
        let mut ranges=
            [c.colour[0]..c.colour[0], c.colour[1]..c.colour[1], c.colour[2]..c.colour[2]];
        for i in 0..self.colours.len() {
            for j in 0..3 {
                let c = self.colours[i].colour[j];
                if c > self.ranges[j].end {self.ranges[j].end = c}
                if c < self.ranges[j].start {self.ranges[j].start = c}
            }
        }
    }

    fn volume(&self) -> f64 {
        let r = &self.ranges;
        (r[0].end - r[0].start)*(r[1].end - r[1].start)*(r[2].end - r[2].start)
    }

    fn len(&self) -> usize {
        self.colours.len()
    }
}

pub fn median_cut_v2(img_raw: &Vec<f64>, length: usize) -> Palette {
    let mut img: Vec<Colour> = img_raw.chunks(3).map(
        |x| Colour::new(x)
    ).collect();
    let mut buckets = vec!(CSlice::from_colour_slice(img.as_mut_slice()));
    for n in 1..length {
        // we could likely achieve some speedup here by putting the buckets in a pqueue or another
        // data structure but it seems unnecessary - this process isn't really the bottleneck here
        let split = ((length as f64) * 0.6) as usize;
        if n < split { // use population only to sort for the first 60%
            buckets.sort_by(|a, b| a.len().cmp(&b.len()))
        } else { // for the last 40% use population * volume to split the sparsest regions of the colour space
            buckets.sort_by(|a, b|
                (a.len() as f64*a.volume()).partial_cmp(&(b.len() as f64 * b.volume())).unwrap_or(Ordering::Equal)
            )
        }

        // find the longest axis and sort the bucket along it
        let largest = buckets.pop().unwrap();
        let mut max_axis = 0;
        let mut max_axis_length = 0.0;
        for i in 0..3 {
            let axis_length = largest.ranges[i].end - largest.ranges[i].start;
            if axis_length > max_axis_length {
                max_axis = i;
                max_axis_length = axis_length;
            }
        }
        largest.colours.sort_by(
            |a, b| a.colour[max_axis].partial_cmp(&b.colour[max_axis]).unwrap_or(Ordering::Equal)
        );

        // here we essentially execute a single iteration of a naive k-means algorithm
        // to sort the data into a 'high' and a 'low' cluster
        let mid = (largest.ranges[max_axis].start + largest.ranges[max_axis].end)/2.0;
        let mut low_sum = 0.0;
        let mut high_sum = 0.0;
        let mut low_count = 0;
        let mut high_count = 0;
        for n in 0..largest.len() {
            let c = largest.colours[n].colour[max_axis];
            if c < mid {
                low_sum += c;
                low_count += 1;
            } else {
                high_sum += c;
                high_count += 1;
            }
        }
        let low_avg = low_sum / (low_count as f64);
        let high_avg = high_sum / (high_count as f64);
        let split = (low_avg + high_avg)/2.0;
        let split_idx = largest.colours.partition_point(
            |x| x.colour[max_axis] < split
        );
        let (low_slice, high_slice) = largest.colours.split_at_mut(split_idx);
        buckets.push(CSlice::from_colour_slice(low_slice));
        buckets.push(CSlice::from_colour_slice(high_slice));
    }
    let mut colours : Vec<Colour> = Vec::with_capacity(length);
    for n in 0..buckets.len() {
        let mut sum = [0., 0., 0.];
        let bucket = &buckets[n];
        for i in 0..bucket.len() {
            for j in 0..3 {sum[j] += bucket.colours[i].colour[j]}
        }
        for j in 0..3 {sum[j] /= bucket.len() as f64}
        colours.push(Colour::new(&sum))
    }
    Palette::new(colours)
}

pub fn greyscale(_: &Vec<f64>, mut length: usize) -> Palette {
    if length <= 1 {length = 2}
    let mut colours : Vec<Colour> = Vec::with_capacity(length);
    let mut n : f64 = 0.0;
    while n < 255.0 {
        let val = n.floor() / 255.0;
        colours.push(Colour::new(&[val, val, val]));
        n += 255.0 / (length as f64 - 1.0);
    }
    colours.push(Colour::new(&[1.0, 1.0, 1.0]));
    Palette::new(colours)
}

pub fn rgb(_img_raw: &Vec<f64>, mut length: usize) -> Palette {
    let mut base: i32 = 1;
    loop {
        if (base+1).pow(3) > length as i32 { break }
        base += 1
    }
    let mut shape = [base, base, base];
    for i in 0..3 {
        let len = shape[0]*shape[1]*shape[2];
        if (len / shape[i]) * (shape[i] + 1) > length as i32 { break }
        shape[i] += 1
    }
    let mut colours : Vec<Colour> = Vec::new();
    let widths: [f64; 3] = [1.0/(shape[0] as f64 - 1.0), 1.0/(shape[1] as f64 - 1.0), 1.0/(shape[2] as f64 - 1.0)];
    for g in 0..shape[0] {
        for r in 0..shape[1] {
            for b in 0..shape[2] {
                let val = [r as f64 * widths[1], g as f64 * widths[0], b as f64 * widths[2]];
                colours.push(Colour::new(&val));
            }
        }
    }
    Palette::new(colours)
}

pub fn kmeans(img_raw: &Vec<f64>, length: usize) -> Palette {
    do_kmeans(img_raw, length, None)
}

pub fn mmc_with_kmeans(img_raw: &Vec<f64>, length: usize) -> Palette {
    let initial = median_cut_v2(img_raw, length);
    do_kmeans(img_raw, length, Some(initial))
}

fn do_kmeans(img_raw: &Vec<f64>, length: usize, init: Option<Palette>) -> Palette {
    let img_raw : Vec<f64> = img_raw.iter().map(|&x| linearise(x)).collect();
    let img = ArrayView::from_shape((img_raw.len()/3, 3), &img_raw).unwrap();
    let img_ds = DatasetBase::from(img.clone());
    let init_method = match init {
        None => KMeansInit::KMeansPlusPlus,
        Some(init) => {
            let init_centroids = init.kdtree.iter().map(|x| x.colour_gamma).collect::<Vec<[f64; 3]>>();
            let array_init: Array2<f64> = init_centroids.into();
            KMeansInit::Precomputed(array_init)
        }
    };
    let kmeans = KMeans::params(length)
        .tolerance(0.5/255.)
        .max_n_iterations(200)
        .fit(&img_ds)
        .unwrap();
    let centroids = kmeans.centroids().as_slice().unwrap();
    let mut colours : Vec<Colour> = Vec::with_capacity(length);
    for i in 0..centroids.len()/3 {
        let mut colour = [0.0, 0.0, 0.0];
        for n in 0..3 {
            colour[n] = centroids[i * 3 + n]
        }
        colours.push(Colour::from_gamma(&colour))
    }
    Palette::new(colours)
}