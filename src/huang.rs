use std::cmp::min;
use std::collections::HashMap;
use crate::img::*;
use crate::stucki;
use rand::prelude::*;

pub fn dither<'a, 'b>(img_raw: &'b Vec<f64>, width: usize, palette: &'a Palette, _dithering_factor: Option<f64>) -> Vec<&'a Colour> {
    let img: Vec<Colour> = img_raw.chunks(3).map(
        |x| Colour::new(x)
    ).collect();
    let height = img.len()/width;
    let (weight_map, norm_factor_map) = weight_map(&img, width);
    let mut colour_map = stucki::dither(img_raw, width, &palette, None);
    let mut diff_map = vec!([0.0, 0.0, 0.0]; img.len());
    let mut error_map = vec!([0.0, 0.0, 0.0]; img.len());
    for i in 0..img.len() {
        for n in 0..3 {
            diff_map[i][n] = colour_map[i].colour_gamma[n] - img[i].colour_gamma[n]
        }
    }
    for idx in 0..img.len() {
        let row = idx / width;
        let col = idx % width;
        let mut error_sum = [0.0, 0.0, 0.0];
        let norm_factor = norm_factor_map.get(&idx).unwrap();
        for i in -1..=1 {
            let offset_col = (col as i32) + i;
            if offset_col >= (width as i32) || offset_col < 0 { continue }
            for j in -1..=1 {
                let offset_row = (row as i32) + i;
                if offset_row >= (height as i32) || offset_row < 0 { continue }
                let offset_idx = offset_col as usize + offset_row as usize * width;
                let coords = if idx < offset_idx { [idx, offset_idx] } else { [offset_idx, idx] };
                let mut weight = *weight_map.get(&coords).unwrap();
                weight = weight / norm_factor;
                for n in 0..3 {
                    error_sum[n] += weight * diff_map[offset_idx][n];
                }
            }
        }
        error_map[idx] = error_sum
    }
    drop(diff_map);

    let mut rng = rand::thread_rng();
    let mut needs_update = vec![true; img.len()];
    for iters in 0..25 {
        let mut order : Vec<usize> = (0..img.len()).collect();
        order.shuffle(&mut rng);
        let mut num_updated = 0;
        for x in 0..img.len() {
            let idx = order[x];
            if !needs_update[idx] { continue }
            let row = idx / width;
            let col = idx % width;
            let mut neighbourhood = Vec::new();
            for i in -1..=1 {
                let offset_col = (col as i32) + i;
                if offset_col >= (width as i32) || offset_col < 0 { continue }
                for j in -1..=1 {
                    let offset_row = (row as i32) + j;
                    if offset_row >= (height as i32) || offset_row < 0 { continue }
                    let offset_idx = offset_col as usize + offset_row as usize * width;
                    neighbourhood.push(offset_idx)
                }
            }
            let mut error_sum = [0.0, 0.0, 0.0];
            let mut weights_sum = 0.0;
            for idx_other in &neighbourhood {
                let idx_other = *idx_other;
                for n in 0..3 {
                    error_sum[n] += error_map[idx_other][n]
                }
                let coords = if idx < idx_other { [idx, idx_other] } else { [idx_other, idx] };
                weights_sum += weight_map.get(&coords).unwrap() / norm_factor_map.get(&idx_other).unwrap()
            }
            let c = colour_map[idx];
            let mut min_error_norm = {
                error_sum[0]*error_sum[0] + error_sum[1]*error_sum[1] + error_sum[2]*error_sum[2]
            };
            let mut min_colour = c;
            let candidate_len = min(palette.len(), 10);
            let candidate_palette = palette.kdtree.nearests(&img[idx], candidate_len);
            for i in 0..candidate_palette.len() {
                let c_other: &Colour = candidate_palette[i].item;
                if c.eq(c_other) { continue }
                let mut error_other = [0.0, 0.0, 0.0];
                for n in 0..3 {
                    error_other[n] = weights_sum * (c_other.colour_gamma[n] - c.colour_gamma[n]) + error_sum[n]
                }
                let error_other_norm = {
                    error_other[0]*error_other[0] + error_other[1]*error_other[1] + error_other[2]*error_other[2]
                };
                if error_other_norm < min_error_norm {
                    min_error_norm = error_other_norm;
                    min_colour = c_other;
                }
            }
            if !c.eq(min_colour) {
                colour_map[idx] = min_colour;
                for idx_other in &neighbourhood {
                    let idx_other = *idx_other;
                    let coords = if idx < idx_other { [idx, idx_other] } else { [idx_other, idx] };
                    let mut weight = *weight_map.get(&coords).unwrap();
                    weight = weight / *norm_factor_map.get(&idx_other).unwrap();
                    for n in 0..3 {
                        error_map[idx_other][n] += weight * (min_colour.colour_gamma[n] - c.colour_gamma[n])
                    }
                    needs_update[idx_other] = true;
                }
                num_updated += 1;
            }
            needs_update[idx] = false;
        }
        if (num_updated as f64 / img.len() as f64) < 0.005 {
            break;
        }
    }
    colour_map
}

pub fn weight_map(image: &[Colour], width: usize) -> (HashMap<[usize; 2], f64>, HashMap<usize, f64>) {
    let height = image.len() / width;
    let mut weight_map : HashMap<[usize; 2], f64> = HashMap::with_capacity(image.len());
    let mut norm_factor_map : HashMap<usize, f64> = HashMap::with_capacity(image.len());
    for col in 0..width {
        for row in 0..height {
            let idx = col + row*width;
            let mut weight_sum = 0.0;
            for i in -1..=1 {
                let offset_col = (col as i32) + i;
                if offset_col >= (width as i32) || offset_col < 0 { continue }
                for j in -1..=1 {
                    let offset_row = (row as i32) + j;
                    if offset_row >= (height as i32) || offset_row < 0 { continue }
                    let offset_idx = offset_col as usize + offset_row as usize *width;
                    let coords = if idx < offset_idx {[idx, offset_idx]} else {[offset_idx, idx]};
                    let weight = match weight_map.get(&coords) {
                        Some(w) => *w,
                        None => {
                            let w = weight([col, row, offset_col as usize, offset_row as usize], [&image[idx], &image[offset_idx]]);
                            weight_map.insert(coords, w);
                            w
                        }
                    };
                    weight_sum += weight
                };
            }
            norm_factor_map.insert(idx, weight_sum);
        }
    }
    (weight_map, norm_factor_map)
}

fn weight(coords: [usize; 4], colours: [&Colour; 2]) -> f64 {
    let sigma_s : f64 = 1.0;
    let sigma_r : f64 = 0.005;
    let dist: f64 = ((coords[0] - coords[2]).pow(2) + (coords[1] - coords[3]).pow(2)) as f64;
    let color_dist = colours[0].dist_sq(&colours[1]);
    (-dist/sigma_s.powi(2)).exp() + (-color_dist/sigma_r.powi(2)).exp()
}
