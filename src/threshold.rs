// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::img::*;

pub fn dither<'a, 'b>(img: &'b Vec<f64>, width: usize, palette: &'a Palette, _dithering_factor: Option<f64>) -> Vec<&'a Colour> {
    let height = img.len()/(width*3);
    let mut colour_map = Vec::with_capacity(img.len()/3);
    for row in 0..height {
        for col in 0..width {
            let idx = row*width*3 + col*3;
            let colour = img[idx..idx+3].to_vec();
            let nearest = palette.nearest(&Colour::new(&*colour));
            colour_map.push(nearest);
        }
    }
    colour_map
}