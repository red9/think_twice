pub mod bayer;
pub mod img;
pub mod knoll;
pub mod stucki;
pub mod quant;
pub mod huang;
pub mod threshold;
