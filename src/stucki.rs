// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::img::*;

const STUCKI: [f64; 12] = [
                8., 4.,
    2., 4., 8., 4., 2.,
    1., 2., 4., 2., 1.
];
const OFFSET: [[i32; 2]; 12] = [
                              [1, 0], [2, 0],
    [-2, 1], [-1, 1], [0, 1], [1, 1], [2, 1],
    [-2, 2], [-1, 2], [0, 2], [1, 2], [2, 2]
];

pub fn dither<'a, 'b>(img_raw: &'b Vec<f64>, width: usize, palette: &'a Palette, dithering_factor: Option<f64>) -> Vec<&'a Colour> {
    let mut error : Vec<f64> = vec![0.0; img_raw.len()];
    let height = img_raw.len()/(3*width);
    let mut img: Vec<&Colour> = Vec::with_capacity(img_raw.len()/3);
    let dithering_factor = match dithering_factor {
        None => 0.8,
        Some(k) => k
    };
    for row in 0..height {
        for col in 0..width {
            let idx = row*width*3 + col*3;
            let mut colour: Vec<f64> = img_raw[idx..idx+3].to_vec();
            for n in 0..3 { colour[n] = linearise(colour[n]) - error[idx+n]};
            let target = palette.nearest(&Colour::from_gamma(&colour));
            let mut this_error: [f64; 3] = [0., 0., 0.];
            for n in 0..3 {
                this_error[n] = (target.colour_gamma[n] - colour[n])*dithering_factor/42.0
            };
            img.push(target);
            for n in 0..12 {
                let offset_row = row as i32 + OFFSET[n][1];
                let offset_col = col as i32 + OFFSET[n][0];
                if offset_row < height as i32 && offset_col < width as i32 && offset_col > -1 {
                    let idx = (offset_row as usize)*width*3 + (offset_col as usize)*3;
                    for i in 0..3 {error[idx+i] += this_error[i] * STUCKI[n]}
                }
            }
        }
    }
    img
}
