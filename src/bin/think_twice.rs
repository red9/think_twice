// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::path::Path;
use image;
use image::{ImageBuffer, RgbImage};
use clap::{load_yaml, App, value_t};
use think_twice::*;
use std::error::Error;

fn main() {
    let yaml = load_yaml!("cli.yaml");
    let m = App::from(yaml).get_matches();

    let in_path = m.value_of("in_path").unwrap();
    let out_path = m.value_of("out_path").unwrap();
    let dither_func = match m.value_of("ditherer") {
        Some("k") | Some("knoll") => knoll::dither,
        Some("b") | Some("bayer") => bayer::dither,
        Some("huang") => huang::dither,
        Some("threshold") => threshold::dither,
        _ => stucki::dither
    };
    let palette_func = match m.value_of("palette") {
        Some("random") => quant::random_palette,
        Some("median_cut") => quant::median_cut,
        Some("mod_median_cut") | Some("mmc") => quant::median_cut_v2,
        Some("greyscale") | Some("grayscale") => quant::greyscale,
        Some("kmeans") => quant::kmeans,
        Some("mmc_kmeans") => quant::mmc_with_kmeans,
        Some("rgb") => quant::rgb,
        _ => quant::cga
    };
    let dithering_factor: Option<f64> = match m.value_of("dithering_factor") {
        None => None,
        Some(s) => Some(s.parse().unwrap())
    };
    let palette_length = value_t!(m, "palette_size", usize).unwrap_or(16);
    let img_file = image::open(in_path).unwrap().into_rgb();
    let width = img_file.width();
    let height = img_file.height();
    let mut img_raw: Vec<f64> = img_file.into_raw().iter().map(|x| (*x as f64) / 255.).collect();
    let palette = palette_func(&img_raw, palette_length);
    let img : Vec<&img::Colour> = dither_func(&mut img_raw, width as usize, &palette, dithering_factor);
    let img_out_raw: Vec<u8> = img.iter().map(|x| x.colour).flatten().map(
        |x| (x * 255.) as u8).collect();
    let img_out: RgbImage = ImageBuffer::from_raw(width, height, img_out_raw).unwrap();
    img_out.save(out_path);
}