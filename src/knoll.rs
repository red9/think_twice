// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::img::*;
use std::cmp::Ordering;

const THRESHOLD: [u8; 64] = [
    0,48,12,60, 3,51,15,63,
    32,16,44,28,35,19,47,31,
    8,56, 4,52,11,59, 7,55,
    40,24,36,20,43,27,39,23,
    2,50,14,62, 1,49,13,61,
    34,18,46,30,33,17,45,29,
    10,58, 6,54, 9,57, 5,53,
    42,26,38,22,41,25,37,21
];

pub fn dither<'a, 'b>(img: &'b Vec<f64>, width: usize, palette: &'a Palette, dithering_factor: Option<f64>) -> Vec<&'a Colour> {
    let dithering_factor = match dithering_factor {
        None => 0.3,
        Some(k) => k
    };
    let height = img.len()/(3*width);
    let mut colour_map = Vec::with_capacity(img.len()/3);
    for row in 0..height {
        for col in 0..width {
            let idx = row*width*3 + col*3;
            let colour = Colour::new(&img[idx..idx+3]);
            let mut candidates: Vec<&Colour> = Vec::with_capacity(64);
            let mut error = [0.0, 0.0, 0.0];
            for i in 0..16 {
                let mut target = [0.0, 0.0, 0.0];
                for n in 0..3 { target[n] = colour.colour_gamma[n] + error[n]*dithering_factor }
                let candidate = palette.nearest(&Colour::from_gamma(&target[..]));
                for n in 0..3 { error[n] += colour.colour_gamma[n] - candidate.colour_gamma[n]}
                candidates.push(candidate);
            }
            candidates.sort_by(|a, b| {
                a.luminance.partial_cmp(&b.luminance).unwrap_or(Ordering::Equal)
            });
            let index = THRESHOLD[(row*8 + col) % 64] as usize;
            colour_map.push(candidates[index / 4]);
        }
    }
    colour_map
}

